<?php namespace Voop\Callpy;

/**
 * Отправить запрос в Callpy
 *
 * Class Callpy
 *
 * @package Voop\Callpy
 */
class Callpy
{
    /**
     * @var string
     */
    private $url = '';


    /**
     * @param \Voop\Callpy\CallpyTransportInterface $transport
     * @param string                                $url
     */
    public function __construct(CallpyTransportInterface $transport, string $url)
    {
        $this->transport = $transport;
        $this->url       = $url;
    }


    /**
     * Постучаться в Callpy и вернуть ответ
     *
     * @param array  $data
     * @param string $url
     * @return array|null
     */
    public function call(array $data, string $url)
    {
        $result = $this->transport->send($this->_makeUrl($url), json_encode($data));

        // ждём json
        if (!$result || !is_string($result)) {
            return null;
        }

        return json_decode($result, true);
    }


    /**
     * @param string $url
     * @return string
     */
    private function _makeUrl(string $url) :string
    {
        return sprintf('%s/%s', $this->url, $url);
    }
}
